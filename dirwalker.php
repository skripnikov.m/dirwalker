<?php

/**
 * Первый вариант обхода директорий и поиска чего нужно (наиболее вменяемый).
 *
 * @param string $folder
 * @param string $regPattern
 *
 * @return Generator
 */
function recursiveSearch(string $folder, string $regPattern): Generator
{
    $directoryIterator = new RecursiveDirectoryIterator($folder);
    $recursiveIterator = new RecursiveIteratorIterator($directoryIterator);
    $files = new RegexIterator($recursiveIterator, $regPattern, RegexIterator::GET_MATCH);

    foreach ($files as $file) {
        yield $file[0];
    }
}

/**
 * Велосипедный вариант для рекурсивного поиска всех файлов одного имени в директории.
 *
 * @param string $dir
 * @param array $array
 * @param string $filename
 *
 * @return void
 */
function recursiveSearchFile(string $dir, array &$array, string $filename): void
{
    $currentDirPaths = array_slice(scandir($dir), 2);

    if (in_array($filename, $currentDirPaths, true)) {
        $array[] = $dir . '/' . $filename;
    } else {
        foreach ($currentDirPaths as $path) {
            recursiveSearchFile($dir . '/' . $path, $array, $filename);
        }
    }
}


$countsSum = 0;
foreach (recursiveSearch('./1', "/.*\/count/") as $countPath) {
    $countsSum += array_sum(explode(' ', file_get_contents($countPath)));
}
print ($countsSum);


$pathsArray = [];
recursiveSearchFile('./1', $pathsArray, 'count');

$countsSum = 0;
foreach ($pathsArray as $countPath) {
    $countsSum += array_sum(explode(' ', file_get_contents($countPath)));
}
print (' ' . $countsSum);